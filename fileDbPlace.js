const fs = require('fs').promises;
const {nanoid} = require('nanoid');
const filename = './dbplace.json';
const unitDb = require('./fileDbUnit');

let unit = [];

let data = [];

module.exports = {
    async init() {
        try {
            const fileContent = await fs.readFile(filename);
            data = JSON.parse(fileContent);
            unit = await unitDb.getLinks();
        } catch (e) {
            console.log(e);
            data = [];
            unit = [];
        }
    },
    async getItems() {
        let returnData = []
        data.map(item => {
            returnData.push({name: item.name, id: item.id});
        })
        return returnData;
    },
    async deleteItem(id) {
        let isUnit = unit.find(item => item.place === id);
        console.log(id, isUnit, unit);
        if (isUnit) {
            return false;
        } else {
            data.splice(data.findIndex(function (i) {
                return i.id === id;
            }), 1);
            return true;
        }
    },
    async addItem(item) {
        item.id = nanoid();
        data.push(item);
        await this.save();
    },
    async getItemById(id) {
        return data.find(item => item.id === id);
    },
    async save() {
        await fs.writeFile(filename, JSON.stringify(data, null, 2));
    }
};
