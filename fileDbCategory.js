const fs = require('fs').promises;
const {nanoid} = require('nanoid');
const filename = './dbcategory.json';

let dataCategory = [];

module.exports = {
    async init() {
        try {
            const fileContent = await fs.readFile(filename);
            dataCategory = JSON.parse(fileContent);
        } catch (e) {
            dataCategory = [];
        }
    },
    async getItems() {
        let returnCategory = []
        dataCategory.map(item => {
            returnCategory.push({name: item.name, id: item.id});
        })

        return returnCategory;

    },
    async deleteItem(id) {
        dataCategory.splice(dataCategory.findIndex(function (i) {
            return i.id === id;
        }), 1);
        return dataCategory;
    },
    async addItem(item) {
        item.id = nanoid();
        dataCategory.push(item);
        await this.save();
    },
    async getItemById(id) {
        return dataCategory.find(item => item.id === id);
    },
    async save() {
        await fs.writeFile(filename, JSON.stringify(dataCategory, null, 2));
    }
};
