const fs = require('fs').promises;
const {nanoid} = require('nanoid');
const filename = './dbunit.json';
const categoriesDb = require('./fileDbCategory');


let data = [];

let categories = [];

module.exports = {
    async init() {
        try {
            const fileContent = await fs.readFile(filename);
            data = JSON.parse(fileContent);
            categories = await categoriesDb.getItems();
          } catch (e) {
            console.log(e);
            data = [];
            categories = [];
            places = [];
        }
    },
    async getItems() {
        let returnData = []
        data.map(item => {
            returnData.push({name: item.name, id: item.id});
        })
        return returnData;
    },
    async getLinks() {
        let returnData = []
        data.map(item => {
            returnData.push({category: item.category, place: item.place});
        })
        return returnData;
    },
    async deleteItem(id) {
        let unit = data.find(item => item.id === id);
        let isCategory = categories.find(item => item.id === unit.category);
        if(isCategory){
            return false;
        } else {
            data.splice(data.findIndex(function(i){
                return i.id === id;
            }), 1);
            return true;
        }
    },
   async addItem(item) {
        item.id = nanoid();
        data.push(item);
        await this.save();
    },
    async getItemById(id) {
        return data.find(item => item.id === id);
    },
    async save() {
        await fs.writeFile(filename, JSON.stringify(data, null, 2));
    }
};
