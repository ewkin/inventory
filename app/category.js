const path = require('path');
const express = require('express');
const multer = require('multer');
const fileDb = require('../fileDbCategory');
const {nanoid} = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    const categories = await fileDb.getItems();
    res.send(categories);
});

router.get('/:id', async (req, res) => {
    const category = await fileDb.getItemById(req.params.id);
    res.send(category);
});

router.delete('/:id', async (req, res)=>{
    await fileDb.deleteItem(req.params.id)
    res.send('Got a DELETE request at '+req.params.id);
});


router.post('/', upload.single('image'), async (req, res) => {
    const category = req.body;
    console.log(category.name);
    if (req.file) {
        category.image = req.file.filename;
    }
    if (category.name) {
        await fileDb.addItem(category);
        res.send(category);
    } else {
        return res.status(400).send({"error": 'Name must be present in the request'});
    }
});

module.exports = router;