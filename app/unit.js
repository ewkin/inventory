const path = require('path');
const express = require('express');
const multer = require('multer');
const fileDb = require('../fileDbUnit');
const {nanoid} = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    const unit = await fileDb.getItems();
    res.send(unit);
});

router.get('/:id', async (req, res) => {
    const unit = await fileDb.getItemById(req.params.id);
    res.send(unit);
});

router.delete('/:id', async (req, res) => {
    const reply = await fileDb.deleteItem(req.params.id)
    if (reply) {
        res.send('Got a DELETE request at ' + req.params.id);
    } else {
        res.send(req.params.id + ' has a link, unable to delete');
    }

});


router.post('/', upload.single('image'), async (req, res) => {
    const unit = req.body;
    if (req.file) {
        unit.image = req.file.filename;
    }
    if (unit.name || unit.category || unit.place) {
        await fileDb.addItem(unit);
        res.send(unit);
    } else {
        return res.status(400).send({"error": 'Name must be present in the request'});
    }
});

module.exports = router;