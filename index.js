const express = require('express');
const cors = require("cors");
const fileDbCategory = require('./fileDbCategory');
const fileDbPlace = require('./fileDbPlace');
const fileDbUnit = require('./fileDbUnit');

const category = require('./app/category');
const place = require('./app/place');
const unit = require('./app/unit');

const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

const port = 8000;

app.use('/category', category);
app.use('/place', place);
app.use('/unit', unit);

const run = async () => {
    await fileDbCategory.init();
    await fileDbPlace.init();
    await fileDbUnit.init();
    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
}
run().catch(console.error);